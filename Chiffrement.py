from tkinter import *
import string
StringFinal=""
stringentreechiffrage=""
MainWindow=Tk()
strourge=StringVar()
clesaisieuser=StringVar()
clesaisieuser.set(8)
#Convert an AsciiCode to a Char
def AsciiToChar(AsciiCode):  
    return chr(AsciiCode)
#Convert a Char to an Ascii Code 
def CharToAscii(Char): 
    return ord(Char)
#Add or Substract the value to the char
def DecallageCaractere(Char, Clef):  
    AsciiCode = CharToAscii(Char)
    NewAsciiCode = DecallageAscii(AsciiCode, Clef)
    NewChar = AsciiToChar(NewAsciiCode)
    AddToString(NewChar)
#Add the value of they key define at the begining
def DecallageAscii(AsciiCode,Value):  
    return int(AsciiCode)+Value 
#Itterate on the string to create the new string
def Decoupage(Chaine, Clef):  
    global StringFinal
    StringFinal=""
    for value in Chaine: 
        DecallageCaractere(value, Clef)
#Concatenate the Char with the all strign 
def AddToString(Char):
    global StringFinal
    StringFinal = StringFinal + Char 

def Chiffrage():
    Decoupage(Entree_chiffrage.get(),int(clesaisieuser.get()))
    strourge.set(StringFinal)
def Dechiffrage():
    Decoupage(Entree_chiffrage.get(),-1*int(clesaisieuser.get()))
    strourge.set(StringFinal)

tableau_minuscule=string.ascii_lowercase
tableau_majuscule=string.ascii_uppercase
tableau_minuscule_decale=tableau_minuscule[int(clesaisieuser.get()):]+tableau_minuscule[:int(clesaisieuser.get())]
tableau_majuscule_decale=tableau_majuscule[int(clesaisieuser.get()):]+tableau_majuscule[:int(clesaisieuser.get())]

MainWindow.geometry("600x600")
MainWindow.resizable(width=False,height=False)
MainWindow.title("Chiffrement de césar")
Entree_chiffrage=Entry(MainWindow,textvariable=stringentreechiffrage)
Entree_chiffrage.grid(column=0,row=0,columnspan=2)
Bouton_chiffrage=Button(MainWindow,text="Chiffrer",command=Chiffrage)
Bouton_chiffrage.grid(column=0,row=2)
Bouton_dechiffrage=Button(MainWindow,text="Dechiffrer",command=Dechiffrage)
Bouton_dechiffrage.grid(column=1,row=2)
Sortie=Label(MainWindow, textvariable=strourge)
Sortie.grid(column=0,row=1,columnspan=2)
SaisieCle=Entry(MainWindow,textvariable=clesaisieuser)
SaisieCle.grid(column=0,row=3)
MainWindow.mainloop()