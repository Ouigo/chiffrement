import unittest
from Chiffrement import *  

class ChiffrementTestCase(unittest.TestCase):
    #def t_testMain(self):
     #   self.assertEqual(Main("ABCD", +8),"IJKL")
      #  self.assertEqual(Main("@[`1", +8),"Hchg") 
    def test_testCharToAscii(self):
        self.assertEqual(CharToAscii("a"),97)
        self.assertEqual(CharToAscii("@"),64) 
    def test_testAsciiToChar(self):
        self.assertEqual(AsciiToChar(56),"8")
        self.assertEqual(AsciiToChar(105),"i")
    def test_testDecallageAscii(self):        
        self.assertEqual(DecallageAscii(97,+8),105)
        self.assertEqual(DecallageAscii(64,-8),56)

